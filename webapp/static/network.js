// The base endpoint to receive data from . See update_url()
var URL_BASE = "http://ec2-54-191-81-252.us-west-2.compute.amazonaws.com";

var width = 960, height = 500;

var svg = d3.select("#graph").append("svg")
    .attr("width", width)
    .attr("height", height);

var force = d3.layout.force()
    .gravity(0.05)
    .distance(100)
    .charge(-100)
    .size([width, height]);

var color = d3.scale.category10();

function update_url() {
    return URL_BASE +
           "?seed=" + document.getElementById("search").value +
           "&organism=" + document.getElementById("organism").value +
           "&type=" + document.getElementById("type").value;
}

// Get the modal
var modal = document.getElementById('myModal');

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    modal.style.display = "none";
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target === modal) {
        modal.style.display = "none";
    }
};

function make_network() {
    var url = update_url();
    svg.selectAll("*").remove();
    d3.json(url, function (error, json) {
        if (error) {
            modal.style.display = "block";
        } else {
            force
                .nodes(json.nodes)
                .links(json.links)
                .start();

            var link = svg.selectAll(".link")
                    .data(json.links)
                    .enter().append("line")
                    .attr("class", function (d) { return "link " + d.type; })
                    .style("stroke-width", function (d) { return d.weight; });

            var node = svg.selectAll(".node")
                    .data(json.nodes)
                    .enter().append("g")
                    .attr("class", "node")
                    .on("dblclick", function (d) { d.fixed = true; })
                    .call(force.drag);

            node.append("circle")
                .attr("r", "5")
                .style("fill", function (d) { return color(d.group); });

            node.append("text")
                .attr("dx", 12)
                .attr("dy", ".35em")
                .style("fill", function (d) { return color(d.group); })
                .text(function (d) { return d.name; });

            force.on("tick", function () {
                link.attr("x1", function (d) { return d.source.x; })
                    .attr("y1", function (d) { return d.source.y; })
                    .attr("x2", function (d) { return d.target.x; })
                    .attr("y2", function (d) { return d.target.y; });

                node.attr("transform", function (d) { return "translate(" + d.x + "," + d.y + ")"; });
            });
        
            var figcap = document.getElementById("legend");
            figcap.innerHTML = "<p>Double-click and drag nodes to pin them to the canvas. Orange genes and dashed edges represent known genetic interactions. Candidate novel interaction partners are in blue, connected by solid edges in the functional network. For clarity, only functional net edges with weights above a log-likelihood score (LLS) cutoff of 3.0 are shown, and functional net nodes are shown if they connect to at least 2 known genetic interaction partners.</p>";
        }
    });
}
