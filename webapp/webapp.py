#!/usr/bin/env python

import os
import sqlite3
import time
from flask import Flask, g, json, jsonify, render_template, request
from flask import send_from_directory


DATABASE = '/home/ubuntu/webapp/netdb.sqlite'

app = Flask(__name__)
app.config.from_object(__name__)


def connect_to_database():
    return sqlite3.connect(app.config['DATABASE'])


def get_db():
    db = getattr(g, 'db', None)
    if db is None:
        db = g.db = connect_to_database()
    return db


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()


def execute_query(query, args=()):
    cur = get_db().execute(query, args)
    ans = cur.fetchone()
    cur.close()
    if ans is not None:
        return ans[0]
    else:
        return 'NOT FOUND'


@app.route("/viewdb")
def viewdb():
    return '<br>'.join(str(row) for row in execute_query(
        """SELECT *
           FROM network
           LIMIT 3"""))


@app.route("/")
def print_data():
    """Respond to a query of the format:
    ?seed=SAE2&organism=Saccharomyces+cerevisiae&type=Dosage+Lethality
    """
    cur = get_db().cursor()
    seedGene = request.args.get('seed')
    org = request.args.get('organism')
    intactType = request.args.get('type')
    result = execute_query(
            """SELECT cluster FROM network WHERE 
               seed = ? AND organism = ? AND type = ? 
               LIMIT 1""",
            (seedGene, org, intactType)
    )
    cur.close()
    if result == 'NOT FOUND':
        return result
    else:
        jsonDict = json.loads(result)
        return jsonify(jsonDict)


@app.route("/network")
def showhome():
    return render_template('network.html')


@app.route("/about")
def showabout():
    return render_template('about.html')


@app.route("/download")
def showdl():
    return render_template('download.html')


@app.route("/datafiles/<filename>")
def download_file(filename):
    return send_from_directory(app.static_folder, filename)


if __name__ == "__main__":
    app.run(debug=True)
