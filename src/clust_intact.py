# -*- coding: utf-8 -*-
"""
Find significant between- and within-cluster genetic interactions

Created on Sat Nov 14 19:11:40 2015

@author: jyoung
"""

import bisect
import collections
import itertools
import networkx as nx
import numpy as np
import os.path
import random
import re
import rpy2.robjects as robj
import scipy.special
import scipy.stats as stats
import statistics
import sys
from rpy2.robjects.packages import importr
from statsmodels.sandbox.stats.multicomp import fdrcorrection0
import biogrid
import genesets


def setup_filepaths(organism):
    """Establish full paths for input gene set file"""
    if organism == 'cerevisiae':
        filepath = os.path.join('..', '..', 'DataProcessed', 
                'Sc_prot_cmplx_Hart2007.txt')
    elif organism == 'pombe':
        filepath = os.path.join('..', '..', 'DataProcessed', 
                'Sp_prot_cmplx_Ryan2013.2col.txt')
    elif organism == 'sapiens':
        filepath = os.path.join('..', '..', 'DataProcessed', 
                'CORUM_Human_Entrez.txt')
    else:
        print('\nORGANISM NOT FOUND. EXITING...\n')
        sys.exit()

    return filepath


def determine_col(organism, clustFile):
    """Determine which gene column in the BIOGRID file to read"""
    gene = open(clustFile).readline().split('\t')[1].rstrip()
    entrezRegEx = re.compile(r'\d+')
    if organism == 'cerevisiae':
        sysNameRegEx = re.compile(r'Y[A-Z][A-Z]\d+')
        ofcSymRegEx = re.compile(r'[A-Z]+')
    elif organism == 'pombe':
        sysNameRegEx = re.compile(r'SP[AB]C\d.')
        ofcSymRegEx = re.compile(r'[a-z]+')
    else:  # organism == 'sapiens'
        sysNameRegEx = re.compile(r'\w+')
        ofcSymRegEx = re.compile(r'[A-Za-z]+.')
    
    if entrezRegEx.match(gene) is not None:
        colName = 'Entrez Gene Interactor A'
    elif sysNameRegEx.match(gene) is not None:
        colName = 'Systematic Name Interactor A'
    elif ofcSymRegEx.match(gene) is not None:
        colName = 'Official Symbol Interactor A'
    else:
        print('ERROR: Unable to match ID type! Exiting...')
        sys.exit()

    return colName


def get_background_probability(intactSet):
    numGenes = len(set(itertools.chain.from_iterable(intactSet)))
    return len(intactSet)/scipy.special.binom(numGenes, 2)


def sparsity_withhold(intactSet, pctWithheld):
    if pctWithheld < 0 or pctWithheld >= 100:
        print('Percent withheld must be >= 0 and < 100. Exiting...')
        sys.exit()
    setSize = len(intactSet)
    numKeep = setSize - round((pctWithheld/100) * setSize)
    
    return set(random.sample(intactSet, numKeep))


def sparsity_withhold_deg(intactSet, degreeCutoff):
    """Withhold genetic interactions based on degree of gene node in 
    interaction network"""
    G = nx.from_edgelist(intactSet)
    node2deg = nx.degree(G)
    removeNodes = [k for k,v in node2deg.items() if v > degreeCutoff]
    G.remove_nodes_from(removeNodes)
    density = nx.density(G)
    avgDegree = statistics.mean(nx.degree(G).values())
    ##print('\nThe density of the filtered interaction network is '\
    ##        '{:.2e}'.format(density))
    ##print('The mean vertex degree is {:.2f}'.format(avgDegree))
    filteredIntactSet = set([frozenset([edge[0], edge[1]]) 
        for edge in nx.to_edgelist(G)])

    return filteredIntactSet


def btw_interact_binom(clust2genes, intactSet, p):
    """Calculate between-cluster interaction from binomial probability"""    
    results = list()
    for i, pair in enumerate(itertools.combinations(clust2genes.keys(), 2)):
        geneset0 = clust2genes[pair[0]]
        geneset1 = clust2genes[pair[1]]
        count = sum(1 for genePair in itertools.product(geneset0, geneset1) 
                if frozenset(genePair) in intactSet)
        n = len(geneset0) * len(geneset1)
        pval = stats.binom.pmf(count, n, p) + stats.binom.sf(count, n, p)
        results.append((pair, pval))
    #print('\nExamined', i+1, 'cluster pairs.')

    return results


def within_interact_binom(clust2genes, intactSet, p):
    """Compute within-cluster interaction from binomial probability"""
    results = list()
    for c in clust2genes.keys():
        count = sum(1 for genePair in itertools.combinations(clust2genes[c], 2) 
                if frozenset(genePair) in intactSet)
        n = scipy.special.binom(len(clust2genes[c]), 2)
        pval = stats.binom.pmf(count, n, p) + stats.binom.sf(count, n, p)
        results.append((c, pval))

    return results


def binomial_model(clust2genes, intactSet, cutoff):
    """Use a binomial model to compute significance of cluster interaction.
    cutoff parameter can be pct pairs to withhold or min/max degree"""
    # NOTE: Background probability to be determined before sparsity withholding
    bkgrdPr = get_background_probability(intactSet)
    
    ##intactSet = sparsity_withhold(intactSet, cutoff)  # withhold % pairs
    ##pctWithheld = cutoff
    origNumPairs = len(intactSet)
    intactSet = sparsity_withhold_deg(intactSet, cutoff)  # min/max degree
    pctWithheld = (origNumPairs - len(intactSet)) / origNumPairs * 100
    
    withinResults = sorted(within_interact_binom(clust2genes, intactSet, 
        bkgrdPr), key=lambda f: f[1])
    withinPvals = [t[1] for t in withinResults]
    withinRejected, withinPvalsCor = fdrcorrection0(withinPvals, is_sorted=True)
    withinSig = np.sum(withinRejected)
    print('{:.2f}'.format(pctWithheld) + ' = ' + str(withinSig))


def permutation_method(clust2genes, intactSet, cutoff):
    """Use permutation method to compute significance of cluster interaction.
    Currently only intended to be used for within-cluster interaction. The 
    permutation occurs by shuffling of genetic interaction network nodes.
    Q-value is used to control FDR."""
    ##intactSet = sparsity_withhold(intactSet, cutoff)  # withhold % pairs
    ##pctWithheld = cutoff
    origNumPairs = len(intactSet)
    intactSet = sparsity_withhold_deg(intactSet, cutoff)  # min/max degree
    pctWithheld = (origNumPairs - len(intactSet)) / origNumPairs * 100
    
    random.seed(a=1)
    NUMRAND = 1000
    clust2cnts = collections.defaultdict(list)
    G = nx.from_edgelist(intactSet)
    shuffleNodes = G.nodes()
    for i in range(NUMRAND):
        random.shuffle(shuffleNodes)
        mapping = dict(zip(G.nodes(), shuffleNodes))
        H = nx.relabel_nodes(G, mapping)
        shuffledIntactSet = set([frozenset(edge) for edge in H.edges()])
        for clust in clust2genes.keys():
            ct = sum(1 for pair in itertools.combinations(clust2genes[clust], 2)
                    if frozenset(pair) in shuffledIntactSet)
            clust2cnts[clust].append(ct)
    pvalues = list()
    for clust in clust2genes.keys():
        clust2cnts[clust].sort()
        trueCt = sum(1 for pair in itertools.combinations(clust2genes[clust], 2)
                if frozenset(pair) in intactSet)
        pval = ( (NUMRAND - bisect.bisect_left(clust2cnts[clust], trueCt) + 1) 
                / (NUMRAND + 1) )
        pvalues.append(pval)
    pvalues.sort()
    
    ##pvalues_r = robj.FloatVector(pvalues)
    ##qvalue_R = importr('qvalue')
    ##qobj_r = qvalue_R.qvalue(pvalues_r, fdr_level=0.05, pi0_method="bootstrap")
    ##numSig = sum(qobj_r.rx2('significant'))
    rejected, pvalsCor = fdrcorrection0(pvalues, is_sorted=True)
    numSig = np.sum(rejected)
    print('{:.2f}'.format(pctWithheld) + ' = ' + str(numSig))


def main():
    organism = sys.argv[1]  # cerevisiae, pombe, sapiens
    intactType = sys.argv[2]  # i.e. Negative Genetic
    modelAns = sys.argv[3]  # binomial, permutation
    cutoff = float(sys.argv[4])  # % withhold, min/max degree
    
    clustFile = setup_filepaths(organism)
    clust2genes = genesets.process_file(clustFile)
    
    colName = determine_col(organism, clustFile)
    intactSet = biogrid.get_interacting_genes(organism, intactType, colName)

    if modelAns == 'binomial':
        binomial_model(clust2genes, intactSet, cutoff)
    else:
        permutation_method(clust2genes, intactSet, cutoff)


if __name__=="__main__":
    main()

