#!/usr/bin/env python

"""
Construct SQL database for predictive functional net clusters

Created: 9 April 2016
"""

import bisect
import func_net_pred
import itertools
import json
import networkx as nx
import os
import pandas as pd
import sqlite3
from networkx.readwrite import json_graph


# List organisms and genetic interaction types to consider
orgstypes = {'Homo sapiens': ('Phenotypic Enhancement', 
                              'Phenotypic Suppression'),
             'Saccharomyces cerevisiae': ('Dosage Growth Defect', 
                                          'Dosage Lethality', 
                                          'Dosage Rescue', 
                                          'Phenotypic Enhancement', 
                                          'Phenotypic Suppression', 
                                          'Synthetic Growth Defect', 
                                          'Synthetic Rescue', 
                                          'Synthetic Lethality'),
             'Drosophila melanogaster': ('Phenotypic Enhancement', 
                                         'Phenotypic Suppression')}

# initialize SQL database
conn = sqlite3.connect( os.path.join('..', 'webapp', 'netdb.sqlite') )
curs = conn.cursor()
curs.execute("""CREATE TABLE network
                (seed TEXT, organism TEXT, type TEXT, cluster TEXT)
             """)

# construct predictive network cluster to store in SQL as JSON
AUC_CUTOFF = 0.70
info = list()
for org in orgstypes:
    if org == 'Drosophila melanogaster':
        LLSCUT = 4.0
    else:
        LLSCUT = 3.0
    species = org.split(' ')[1]
    biogridpath, fnetpath = func_net_pred.setup_filepaths(species)
    funcNetDf = pd.read_pickle(fnetpath)
    geneExample = funcNetDf.columns[0]
    colName = func_net_pred.determine_col(species, geneExample)
    
    if org == 'Homo sapiens':  # read in gene ID conversion table for human
        convertPath = os.path.join('..', '..', 'DataProcessed', 
                'org.Hs.egSYMBOL.txt')
        entrez2convert = dict( line.strip().split('\t') for line in 
                open(convertPath) )
    elif org == 'Drosophila melanogaster':  # gene ID conversion for fly
        convertPath = os.path.join('..', '..', 'DataProcessed', 
                'org.Dm.egGENENAME.txt')
        entrez2convert = dict( line.strip().split('\t') for line in 
                open(convertPath) )
    else:  # not converting yeast (should already be symbol)
        pass
    
    for intactType in orgstypes[org]:
        seedSets = func_net_pred.read_biogrid(biogridpath, intactType, colName)
        seedAUC, seed2intacts = func_net_pred.seed_set_predictability(funcNetDf, 
                                                                      seedSets)
        AUCs = [t[0] for t in seedAUC]
        cutpos = bisect.bisect_left(AUCs, AUC_CUTOFF)
        seedGenes = [t[1] for t in seedAUC[cutpos:]]  # get predictive seeds
        
        for gene in seedGenes:
            G = nx.Graph()
            interactors = seed2intacts[gene]
            TF = funcNetDf[interactors] > LLSCUT
            keepPartners = set(TF.index[TF.sum(axis=1) > 1])
            useNodes = list(keepPartners) + interactors
            finalNet = funcNetDf.loc[useNodes, useNodes].copy()
            s = finalNet[finalNet > LLSCUT].stack()
            if gene.isdigit():
                try:
                    seedName = entrez2convert[gene]
                except:
                    continue
            else:
                seedName = gene
            for row in s.iteritems():
                gene0 = row[0][0]
                gene1 = row[0][1]
                if gene0.isdigit():
                    node1 = entrez2convert[gene0]
                else:
                    node1 = gene0
                if gene1.isdigit():
                    node2 = entrez2convert[gene1]
                else:
                    node2 = gene1
                G.add_edge(node1, node2)
                G[node1][node2]['weight'] = row[1]
            for e in itertools.product([seedName], interactors):
                if e[1].isdigit():
                    partnerNode = entrez2convert[e[1]]
                else:
                    partnerNode = e[1]
                G.add_edge(e[0], partnerNode)
                G.edge[e[0]][partnerNode]['type'] = 'dash'
                G.node[partnerNode]['group'] = 0
            G.node[seedName]['group'] = 0
            for n in G:
                G.node[n]['name'] = n
            jsonNet = json_graph.node_link_data(G)
            info.append( (seedName, org, intactType, json.dumps(jsonNet)) )

# write into database
curs.executemany("INSERT INTO network VALUES (?,?,?,?)", info)
curs.execute("CREATE INDEX idx1 ON network(seed)")
conn.commit()
conn.close()

