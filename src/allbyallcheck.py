#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Given an organism and genetic interaction type, check if screens in BIOGRID are 
all-by-all

Created: 17 September 2016
"""

import itertools
import os
import sys


def get_biogrid_filename(species):
    """Get name of BIOGRID file given the species"""
    biogridFolder = os.path.join('..', '..', 'DataDownload', 'BIOGRID', 
            'BIOGRID-ORGANISM-3.4.130.tab2')
    biogridFile = ''
    for dirpath, dirnames, filenames in os.walk(biogridFolder):
        for fname in filenames:
            if species in fname:
                biogridFile = fname
                break
    if biogridFile == '':
        print('BIOGRID file for species not found!')
        sys.exit()

    return os.path.join(biogridFolder, biogridFile)


def get_studies(biogridPath, intactType):
    """Retrieve list of studies in BIOGRID for given organism and genetic 
    interaction type. At the same time, create a dictionary mapping each study 
    to its screened gene pairs."""
    biogridFile = open(biogridPath)
    header = biogridFile.readline().split('\t')
    studyCol = header.index('Pubmed ID')
    expSysCol = header.index('Experimental System')
    studies = dict()
    for line in biogridFile:
        fields = line.split('\t')
        if fields[expSysCol] == intactType:
            pubmedID = fields[studyCol]
            interactorA = fields[1]
            interactorB = fields[2]
            studies.setdefault(pubmedID, set()).add( 
                    (interactorA, interactorB) )
    biogridFile.close()

    return studies


def check_study_allbyall(studies):
    """Assert that number of gene pairs is equal to the number of possible 
    pairs from the gene list"""
    print('Studies with all-by-all screens:')
    for pubmedID in studies:
        uniqueGenes = set( itertools.chain.from_iterable(studies[pubmedID]) )
        numPosPairs = len( list(itertools.combinations(uniqueGenes, 2)) )
        if numPosPairs == len(studies[pubmedID]):
            print( '{}: {}'.format(pubmedID, len(studies[pubmedID])) )


def main():
    species = sys.argv[1]
    intactType = sys.argv[2]
    biogridPath = get_biogrid_filename(species)
    studies = get_studies(biogridPath, intactType)
    check_study_allbyall(studies)


if __name__=="__main__":
    main()

