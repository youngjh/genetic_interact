# genetic_interact

## Introduction

Predict genetic interactions using functional gene networks across various
eukaryotic organisms.

## Software Requirements

Python 3 along with NumPy, NetworkX, scikit-learn

## File descriptions

*func_net_pred.py* discoveries of known genetic interactions to predict 
additional interactions from the functional gene network

## Licencse

See [LICENSE](LICENSE.txt)
